import cv2
import numpy as np
vid = cv2.VideoCapture(0)
vid.set(10,.05) #set brightness to

while(True):
    ret, frame = vid.read()
    # Create the mask that will be used to show single out the targets
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    lower_green = np.array([75,200,200])
    upper_green = np.array([85,255,255])
    mask = cv2.inRange(hsv, lower_green, upper_green)

    # Take frame (original image), perform a bitwise & on it along with the mask.
    # This means that whatever is not in the mask will be removed from frame
    # and placed into res
    res = cv2.bitwise_and(frame,frame,mask=mask)

    # Show the original and output
    cv2.imshow('orig',frame)
    cv2.imshow('fff',res)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
vid.release()
cv2.destroyAllWindows()
