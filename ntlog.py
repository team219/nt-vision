import logging
#from networktables import NetworkTables
import sys

logging.basicConfig(filename='nt.log',format='[%(levelname)s] %(message)s',level=logging.DEBUG)
logging.info('===STARTING===')
logging.info('VERSION: %s',sys.version)
logging.debug('NT setClientMode')
#NetworkTables.setClientMode()
logging.debug('NT setTeam(219)')
#NetworkTables.setTeam(219)
logging.debug('NT getTable(\'vision\')')
#table = NetworkTables.getTable('vision')
logging.info('===ENDING===')
