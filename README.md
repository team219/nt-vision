# Network Tables/Vision

* * *
## Autostart CV Env on Pi
```
sudo nano ~/.bashrc
	export WORKON_HOME=~/.virtualenvs
	VIRTUALENVWRAPPER_PYTHON='/usr/bin/python2.7'
	source /usr/local/bin/virtualenvwrapper.sh
sudo nano ~/.profile
	workon cv
```

```
sudo nano ~/etc/rc.local
        (sleep 10;python scriptname.py)&
```

## Shutting down pi on robot
* Download [Cygwin64](https://cygwin.com/install.html)
* Open Cygwin64 terminal and type 
```
#!bash

ssh pi@[ip addr] 'sudo shutdown -h now'
```

## Helpful links
```
https://github.com/kevinabrandon/StrongholdTargetFinder/blob/master/00-ShowCamera.py
http://picamera.readthedocs.io/en/release-1.10/_modules/picamera/array.html
http://picamera.readthedocs.io/en/release-1.9/recipes1.html
```